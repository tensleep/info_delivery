package com.infodeli.common.enums;

public enum LocalAuthEnum {
    LOCAL_AUTH_PHONE(1),
    LOCAL_AUTH_EMAIL(2);

    private Integer authType;

    LocalAuthEnum(Integer authType){
        this.authType = authType;
    }

    public Integer getAuthType(){
        return authType;
    }
}
