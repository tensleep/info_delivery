package com.infodeli.common.utils;

import com.infodeli.common.exception.ExceptionEnum;
import com.infodeli.common.exception.ServiceException;

public class ServiceExceptionUtils {

    public static ServiceException getServiceException(ExceptionEnum exceptionEnum){
        ServiceException exception = new ServiceException(exceptionEnum.getCode(), exceptionEnum.getMsg());
        return exception;
    }
}
