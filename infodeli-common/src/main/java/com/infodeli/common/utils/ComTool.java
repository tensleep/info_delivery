package com.infodeli.common.utils;

import com.infodeli.common.exception.ExceptionEnum;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class ComTool {

    // 日期格式化为字符串“xxxx/xx/xx xx:xx:xx”
    public static String getDateStr(){
        // 格式化器
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

        // 日期时间转字符串
        LocalDateTime now = LocalDateTime.now();
        String nowText = now.format(formatter);

        return nowText;
    }
    // 时间格式化为 简单字符串
    public static String getSimpleDateStr(){
        // 格式化器
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMddHHmmss");
        // 日期时间转字符串
        LocalDateTime now = LocalDateTime.now();
        String nowText = now.format(formatter);
        return nowText;
    }

    // 日期格式化为字符串“xxxx/xx/xx xx:xx:xx”
    public static String getVeriCodeDateStr(){
        // 格式化器
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

        // 日期时间转字符串
        LocalDateTime now = LocalDateTime.now();
        now = now.plusMinutes(10);
        String dateStr = now.format(formatter);

        return dateStr;
    }

    public static String getVeriCode(){
        Random random = new Random();
        int i = random.nextInt(1000000);
        String veriCode = String.format("%06d", i);
        return veriCode;
    }

    // 获取md5加密后的数据
    public static String getMd5Str(String source){
        String dist = DigestUtils.md5DigestAsHex(source.getBytes());
        return dist;
    }

}
