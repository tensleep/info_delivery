package com.infodeli.common.utils;

import java.util.Map;

public class MyMapUtils {

    public static void show(Map<String, Object> map){
        for(Map.Entry<String, Object> entry : map.entrySet()){
            System.out.println(entry.getKey() + "======" + (String) entry.getValue());
        }
    }
}
