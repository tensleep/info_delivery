package com.infodeli.common.exception;

public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String msg;
    private Integer code;

    public ServiceException(Integer code, String msg) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public ServiceException() {
        super();
    }

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }
}
