package com.infodeli.common.exception;

public class LoginException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String msg;
    private Integer code;

    public LoginException(Integer code, String msg) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public LoginException() {
        super();
    }

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }
}
