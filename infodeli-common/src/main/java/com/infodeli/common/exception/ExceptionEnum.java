package com.infodeli.common.exception;

public enum ExceptionEnum {

    // login 100
    LOGIN(10000, "登录成功"),
    PASSWORD_ERROR(10001,"密码错误"),
    USER_NOT_EXIST(10002, "用户不存在"),
    USER_BAN(10003,"用户封禁"),
    USERNAME_EMPTY(10004, "请输入用户名"),
    USERNAME_ERROR(10005, "用户名在9~10位"),
    PASSWORD_EMPTY(10006,"请输入密码"),
    PASSWORD_MIN_SIX(10007,"密码至少六位"),

    // token 100
    USER_TOKEN_ERROR(10010, "用户token异常"),
    USER_TOKEN_EXPIRE(10011,"token过期"),
    USER_TOKEN_ALGORITHM(10012,"token加解密算法不匹配"),
    USER_TOKEN_SIGNATURE(10013,"token签名验证出错"),
    USER_TOKEN_NOT_EXIST(10014,"需要提供token"),

    // register 101
    REGISTER(10100, "注册成功"),
    USER_EXIST(10101, "用户名已存在"),

    // local_auth 102
    LOCAL_AUTH_UNBIND(10200 , "本地认证解绑成功"),
    LOCAL_AUTH_BIND(10201, "本地认证绑定成功"),
    LOCAL_AUTH_NOT_EXIST(10202, "本地认证信息不存在"),
    LOCAL_AUTH_VERIFY_CODE_ERROR(10203, "验证码不正确"),
    LOCAL_AUTH_OVERTIME(10204, "验证码超时"),
    LOCAL_AUTH_UNKNOWN_TYPE(10205, "未知本地认证类型"),
    LOCAL_AUTH_USED(10206, "当前手机号/邮箱已被他人使用"),
    LOCAL_AUTH_PASSWORD_ERROR(10207, "密码错误"),
    LOCAL_AUTH_SUCCESS(10210, "本地认证成功"),

    // format 110
    FORMAT_PHONE(11001, "手机号格式异常"),
    FORMAT_EMAIL(11002, "邮箱格式异常"),



    // error
    ALL_ERROR(99999, "未知异常");

    private String msg;
    private Integer code;

    ExceptionEnum(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode(){
        return code;
    }
    public String getMsg(){
        return msg;
    }

}
