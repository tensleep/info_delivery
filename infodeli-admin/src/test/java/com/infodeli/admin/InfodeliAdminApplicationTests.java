package com.infodeli.admin;

import com.infodeli.InfodeliAdminApplication;
import com.infodeli.admin.entity.InfoCategoryEntity;
import com.infodeli.admin.service.InfoCategoryService;
import com.infodeli.admin.service.InfoService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.user.dialog.service.DialogDetailService;
import com.infodeli.user.dialog.service.DialogRelationService;
import com.infodeli.user.login.entity.UserEntity;
import com.infodeli.user.login.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Array;
import java.util.*;

@SpringBootTest
class InfodeliAdminApplicationTests {

	@Autowired
	InfoService infoService;

	@Autowired
	InfoCategoryService infoCategoryService;

	//@Qualifier("zzsuserService")
	@Autowired
	UserService userService;

	@Autowired
	DialogRelationService dialogRelationService;

	@Autowired
	DialogDetailService dialogDetailService;
	/*
	"page":{
        "key":"威海",
        "limit": "6"
    },
    "infoStatus":[1,2,0,-1], //[0-待,1-审核,2-成功,-1-删,-2-失败]
    "userId": "1",
    "useUserId": "1",
    "catId": "5"
	* */
	@Test
	void testQueryPageInfoVos(){
		Map<String, Object> params = new HashMap<>();
		Map<String, Object> page = new HashMap<>();
		page.put("page","1");
		page.put("limit", "5");
		//page.put("key", "威海");
		List<Integer> showStatus = new ArrayList<>();
		showStatus.add(1);
		showStatus.add(2);
		params.put("page",page);
		params.put("infoStatus", showStatus);
		params.put("userId", "1");
		params.put("useUserId", "1");
		params.put("catId", "5");
		PageUtils pageUtils = infoService.queryPageVo(params);
		System.out.println("size: "+pageUtils.getList().size());
		pageUtils.getList().forEach(entity ->{
			System.out.println(entity);
		});
	}

	@Test
	void testBeanNameConflict(){
		System.out.println(dialogDetailService.getClass());
	}


	@Test
	void contextLoads() {
		//List<InfoCategoryEntity> list = infoCategoryService.list();
		//for(InfoCategoryEntity ic : list){
		//	System.out.println(ic);
		//}
		InfoCategoryEntity entity = new InfoCategoryEntity();
		entity.setCatId(1L);
		entity.setParentCid(2L);
		infoCategoryService.updateById(entity);
	}

	@Test
	void testFindCatIdPath(){
		Long catId = 19L;
		List<Long> catIdPath = infoCategoryService.findCatIdPath(catId);
		System.out.println("查询"+catId+"的路径为"+ catIdPath);
	}

	@Test
	void testAllUser(){
		//List<UserEntity> userEntities = userService.list();
		//for(UserEntity entity : userEntities){
		//	System.out.println(entity);
		//}

		System.out.println(userService.getClass());
		System.out.println("hello");
	}



}
