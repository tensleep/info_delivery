package com.infodeli.admin;

import com.infodeli.admin.service.InfoCommentReplyService;
import com.infodeli.admin.service.InfoCommentService;
import com.infodeli.admin.service.InfoService;
import com.infodeli.admin.vo.InfoCommentReplyVo;
import com.infodeli.admin.vo.InfoCommentVo;
import com.infodeli.common.utils.PageUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InfoCommentTest {

    @Autowired
    InfoService infoService;

    @Autowired
    InfoCommentService infoCommentService;

    @Autowired
    InfoCommentReplyService infoCommentReplyService;

    @Test
    public void testDeleteLogicComment(){
        infoCommentService.deleteLogic(3L,2L,4L);
    }

    @Test // 测试 删除 reply
    public void testDeleteLogicReply(){
        infoCommentReplyService.deleteLogic(3L,2L,4L,3L);
    }

    @Test // 增加消息的评论
    public void testAddCommentNum(){
        infoService.addCommentNum(4L, 4);
    }

    @Test // 增加 回复的数量
    public void testAddReplyNum(){
        infoCommentService.addReplyNum(4L, 3L, 2);
    }

    @Test // 查询评论
    public void testQueryPageByInfoId(){
        Map<String, Object> params = new HashMap<>();
        params.put("page", "1");
        params.put("limit", "2");
        params.put("sidx", "create_time");
        params.put("order", "asc");
        PageUtils pageUtils = infoCommentService.queryPageByInfoId(params, 1L);
        List<InfoCommentVo> infoCommentVos = (List<InfoCommentVo>) pageUtils.getList();
        infoCommentVos.forEach(vo ->{
            System.out.println(vo);
        });
        //System.out.println("test test");
    }

    @Test // 查询回复
    public void testQueryPageByCommentId(){
        Map<String, Object> params = new HashMap<>();
        params.put("page", "1");
        params.put("limit", "3");
        params.put("sidx", "create_time");
        params.put("order", "asc");
        PageUtils pageUtils = infoCommentReplyService.queryPageByCommentId(params, 3L);
        List<InfoCommentReplyVo> infoCommentReplyVos = (List<InfoCommentReplyVo>) pageUtils.getList();
        infoCommentReplyVos.forEach(vo -> {
            System.out.println(vo);
        });
    }
}
