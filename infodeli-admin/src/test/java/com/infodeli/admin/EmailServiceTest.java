package com.infodeli.admin;

import com.infodeli.user.login.entity.CodeEmailEntity;
import com.infodeli.user.login.service.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EmailServiceTest {

    @Autowired
    EmailService emailService;

    @Test
    public void sendEmailTest(){
        CodeEmailEntity codeEmailEntity = new CodeEmailEntity();
        codeEmailEntity.setCode("123456");
        //codeEmailEntity.setTo("1049753744@qq.com");
        codeEmailEntity.setTo("zzshhh521@163.com");
        codeEmailEntity.setSubject("校园信息发布平台 邮箱绑定验证");
        codeEmailEntity.setContent("欢迎使用校园信息发布平台，您的验证码是：");
        emailService.sendCodeEmail(codeEmailEntity);
        System.out.println("邮件发送成功！！！");
    }
}
