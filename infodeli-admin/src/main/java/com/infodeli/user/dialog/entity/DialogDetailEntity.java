package com.infodeli.user.dialog.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-19 16:06:26
 */
@Data
@TableName("dialog_detail")
public class DialogDetailEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 对话信息详情id
	 */
	@TableId
	private Long detailId;
	/**
	 * 对话关系id
	 */
	private Long dialId;
	/**
	 * 发送内容
	 */
	private String content;
	/**
	 * 创建时间
	 */
	private String createTime;
	/**
	 * [0:发->接,1:接->发]
	 */
	private Integer sendStatus;
	/**
	 * 是否显示[0-否,1是]
	 */
	private Integer showStatus;
	/**
	 * 是否已读[0-否,1是]
	 */
	private Integer readStatus;

	// 消息发送者id
	private Long userSend;

	@TableField(exist = false)
	private String userSendNickName;

}
