package com.infodeli.user.dialog.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.infodeli.user.dialog.entity.DialogRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-19 16:06:27
 */
@Mapper
public interface DialogRelationDao extends BaseMapper<DialogRelationEntity> {

    IPage<DialogRelationEntity> queryPageAdmin(IPage<?> page, @Param("ew") Wrapper<DialogRelationEntity> queryWrapper);
}
