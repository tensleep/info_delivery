package com.infodeli.user.dialog.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-19 16:06:27
 */
@Data
@TableName("dialog_relation")
public class DialogRelationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 对话关系id
	 */
	@TableId
	private Long dialId;
	/**
	 * 发起方id
	 */
	private Long userSend;
	/**
	 * 接收方id
	 */
	private Long userRecv;
	/**
	 * 发起时间
	 */
	private String createTime;
	/**
	 * 更新时间
	 */
	private String newTime;
	/**
	 * 是否显示[0-否,1是]
	 */
	private Integer showStatusSend;
	private Integer showStatusRecv;
	/**
	 * 未读者id
	 */
	private Long unreadId;
	/**
	 * 未读信息个数
	 */
	private Integer unreadNum;

	private String newContent;

	@TableField(exist = false)
	private String userSendNickName;
	@TableField(exist = false)
	private String userRecvNickName;

}
