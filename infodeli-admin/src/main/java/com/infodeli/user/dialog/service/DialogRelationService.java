package com.infodeli.user.dialog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.user.dialog.entity.DialogRelationEntity;
import com.infodeli.user.dialog.vo.DialogRelationVo;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-19 16:06:27
 */
public interface DialogRelationService extends IService<DialogRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByUid(Map<String, Object> params, Long uid);

    void addDialogRelation(Long fromId, Long toId);

    void deleteByUid(Long dialId, Long uid);

    DialogRelationVo getVoById(Long dialId, Long uid);
}

