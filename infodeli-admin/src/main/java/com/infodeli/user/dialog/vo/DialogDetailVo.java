package com.infodeli.user.dialog.vo;

import lombok.Data;

@Data
public class DialogDetailVo {
    private Long detailId;

    private String content;

    private String createTime;

    private Integer isMine;

    private Integer readStatus;

    private Long userSend;

    private String userSendNickName;
}
