package com.infodeli.user.dialog.dao;

import com.infodeli.user.dialog.entity.DialogDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-19 16:06:26
 */
@Mapper
public interface DialogDetailDao extends BaseMapper<DialogDetailEntity> {
	
}
