package com.infodeli.user.dialog.controller;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.infodeli.common.utils.JWTUtils;
import com.infodeli.component.bean.Message;
import com.infodeli.user.dialog.vo.DialogDetailVo;
import com.infodeli.user.login.entity.UserEntity;
import org.apache.http.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.infodeli.user.dialog.entity.DialogDetailEntity;
import com.infodeli.user.dialog.service.DialogDetailService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * 
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-19 16:06:26
 */
@RestController
@RequestMapping("dialog/detail")
public class DialogDetailController {
    @Autowired
    private DialogDetailService dialogDetailService;

    /**
     * 查询：具体对话的详细信息（无分页）
     * @param dialogId ：某个对话
     * @param request : 获取toke中的 用户信息，来获取当前用户
     * @return
     */
    @PostMapping("/{dialogId}")
    public R listByDialogId(@PathVariable("dialogId") Long dialogId, HttpServletRequest request) throws UnsupportedEncodingException {
        //UserEntity user = (UserEntity)session.getAttribute("user");
        String token = request.getHeader("token");
        Long uid = Long.parseLong(JWTUtils.getTokenInfo(token, "uid"));
        List<DialogDetailVo> list = dialogDetailService.getDetailByDialogId(dialogId, uid );


        return R.ok().put("detail",list);
    }

    /**
     * 查询：按照 detailId 分页 【占领】
     */
    @PostMapping("/page/{dialogId}/{detailId}")
    public R pageListByDialogId(@RequestBody Map<String, Object> params,
                                @PathVariable("dialogId") Long dialogId,
                                @PathVariable("detailId") Long detailId,
                                HttpServletRequest request) throws UnsupportedEncodingException {

        String token = request.getHeader("token");
        Long uid = Long.parseLong(JWTUtils.getTokenInfo(token, "uid"));
        PageUtils page = dialogDetailService.queryPagByDialogId(params, dialogId, detailId, uid );


        return R.ok().put("page",page);
    }

    /**
     * 假设这个是 ws 的服务器
     * @return
     */
    @PostMapping("/send")
    public R send(@RequestBody Message message){
        dialogDetailService.addMessage(message);
        return R.ok();
    }



    /**
     * 列表：分页查询【管理员占领】
     */
    @RequestMapping("/list/{dialId}")
    //@RequiresPermissions("dialog:dialogdetail:list")
    public R list(@RequestBody Map<String, Object> params, @PathVariable("dialId") Long dialId){
        PageUtils page = dialogDetailService.queryPageAdmin(params, dialId);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{detailId}")
    //@RequiresPermissions("dialog:dialogdetail:info")
    public R info(@PathVariable("detailId") Long detailId){
		DialogDetailEntity dialogDetail = dialogDetailService.getById(detailId);

        return R.ok().put("dialogDetail", dialogDetail);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("dialog:dialogdetail:save")
    public R save(@RequestBody DialogDetailEntity dialogDetail){
		dialogDetailService.save(dialogDetail);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("dialog:dialogdetail:update")
    public R update(@RequestBody DialogDetailEntity dialogDetail){
		dialogDetailService.updateById(dialogDetail);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("dialog:dialogdetail:delete")
    public R delete(@RequestBody Long[] detailIds){
		dialogDetailService.removeByIds(Arrays.asList(detailIds));

        return R.ok();
    }

}
