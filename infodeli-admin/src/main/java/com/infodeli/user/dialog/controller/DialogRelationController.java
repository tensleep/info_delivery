package com.infodeli.user.dialog.controller;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.infodeli.common.utils.JWTUtils;
import com.infodeli.user.dialog.vo.DialogRelationVo;
import org.apache.http.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.infodeli.user.dialog.entity.DialogRelationEntity;
import com.infodeli.user.dialog.service.DialogRelationService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;

import javax.servlet.http.HttpServletRequest;


/**
 * 
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-19 16:06:27
 */
@RestController
@RequestMapping("dialog/relation")
public class DialogRelationController {
    @Autowired
    private DialogRelationService dialogRelationService;

    /**
     * 查询：分页查询 当前用户 相关的对话【占领】
     */
    @PostMapping("/{uid}")
    public R userDialogRelation( @RequestBody Map<String, Object> params, @PathVariable("uid") Long uid){
        PageUtils page = dialogRelationService.queryPageByUid(params, uid);
        return R.ok().put("page", page);
    }


    /**
     * 列表：查询所有 对话关系【管理员占领】
     */
    @RequestMapping("/list")
    //@RequiresPermissions("dialog:dialogrelation:list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = dialogRelationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息：某一个对话关系的信息 【占领】
     */
    @RequestMapping("/info/{dialId}")
    //@RequiresPermissions("dialog:dialogrelation:info")
    public R info(@PathVariable("dialId") Long dialId, HttpServletRequest request) throws UnsupportedEncodingException {
        Long uid = Long.parseLong(JWTUtils.getTokenInfo(request.getHeader("token"), "uid"));
        DialogRelationVo vo = dialogRelationService.getVoById(dialId, uid);
        //DialogRelationEntity dialogRelation = dialogRelationService.getById(dialId);

        return R.ok().put("dialogRelation", vo);
    }

    /**
     * 保存：发起对话：新建 or 修改showStatus【占领】
     */
    @RequestMapping("/save/{toId}")
    //@RequiresPermissions("dialog:dialogrelation:save")
    public R save(@PathVariable("toId")Long toId, HttpServletRequest request) throws UnsupportedEncodingException {
        Long fromId = Long.parseLong(JWTUtils.getTokenInfo(request.getHeader("token"), "uid"));
		//dialogRelationService.save(dialogRelation);
        dialogRelationService.addDialogRelation(fromId, toId);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("dialog:dialogrelation:update")
    public R update(@RequestBody DialogRelationEntity dialogRelation){
		dialogRelationService.updateById(dialogRelation);

        return R.ok();
    }

    /**
     * 删除：修改 对话关系 中某个用户 的 showStatus 【占领】
     */
    @RequestMapping("/delete/{dialId}")
    //@RequiresPermissions("dialog:dialogrelation:delete")
    public R delete(@PathVariable("dialId")Long dialId, HttpServletRequest request) throws UnsupportedEncodingException {
		//dialogRelationService.removeByIds(Arrays.asList(dialIds));
        Long uid = Long.parseLong(JWTUtils.getTokenInfo(request.getHeader("token"), "uid"));
        dialogRelationService.deleteByUid(dialId, uid);

        return R.ok();
    }

}
