package com.infodeli.user.dialog.vo;

import lombok.Data;

@Data
public class DialogRelationVo {
    private Long dialId;
    private Long uid;
    private String nickName;
    private String portrait;
    private Integer unreadNum;
    private String newContent;
}
