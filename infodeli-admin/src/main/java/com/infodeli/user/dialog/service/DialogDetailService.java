package com.infodeli.user.dialog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.component.bean.Message;
import com.infodeli.user.dialog.entity.DialogDetailEntity;
import com.infodeli.user.dialog.vo.DialogDetailVo;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-19 16:06:26
 */
public interface DialogDetailService extends IService<DialogDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<DialogDetailVo> getDetailByDialogId(Long dialogId, Long uid);

    PageUtils queryPagByDialogId(Map<String, Object> params, Long dialogId, Long detailId, Long uid);

    void addMessage(Message message);


    PageUtils queryPageAdmin(Map<String, Object> params, Long dialId);
}

