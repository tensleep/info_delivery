package com.infodeli.user.login.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.Query;

import com.infodeli.user.login.dao.UserLoginRecordDao;
import com.infodeli.user.login.entity.UserLoginRecordEntity;
import com.infodeli.user.login.service.UserLoginRecordService;


@Service("userLoginRecordService")
public class UserLoginRecordServiceImpl extends ServiceImpl<UserLoginRecordDao, UserLoginRecordEntity> implements UserLoginRecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserLoginRecordEntity> page = this.page(
                new Query<UserLoginRecordEntity>().getPage(params),
                new QueryWrapper<UserLoginRecordEntity>()
        );

        return new PageUtils(page);
    }

}