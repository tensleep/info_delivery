package com.infodeli.user.login.entity;

import lombok.Data;

@Data
public class CodeEmailEntity {
    private String subject;
    private String content;
    private String code;
    private String to;
}
