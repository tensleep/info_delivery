package com.infodeli.user.login.service;

import com.infodeli.user.login.entity.CodeEmailEntity;

public interface EmailService {
    void sendCodeEmail(CodeEmailEntity codeEmailEntity);
}
