package com.infodeli.user.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.user.login.entity.UserEntity;

import java.util.Map;

/**
 * 
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-12 19:36:06
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);

    UserEntity doLogin(UserEntity user);

    Map<String, String> getUserMap(UserEntity user);


    void register(UserEntity user);

    void authPassword(UserEntity user);
}

