package com.infodeli.user.login.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.infodeli.user.login.entity.UserLoginRecordEntity;
import com.infodeli.user.login.service.UserLoginRecordService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;



/**
 * 用户登录记录表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-29 13:33:05
 */
@RestController
@RequestMapping("user/login/record")
public class UserLoginRecordController {
    @Autowired
    private UserLoginRecordService userLoginRecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("login:userloginrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = userLoginRecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("login:userloginrecord:info")
    public R info(@PathVariable("id") Long id){
		UserLoginRecordEntity userLoginRecord = userLoginRecordService.getById(id);

        return R.ok().put("userLoginRecord", userLoginRecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("login:userloginrecord:save")
    public R save(@RequestBody UserLoginRecordEntity userLoginRecord){
		userLoginRecordService.save(userLoginRecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("login:userloginrecord:update")
    public R update(@RequestBody UserLoginRecordEntity userLoginRecord){
		userLoginRecordService.updateById(userLoginRecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("login:userloginrecord:delete")
    public R delete(@RequestBody Long[] ids){
		userLoginRecordService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
