package com.infodeli.user.login.service.impl;

import com.infodeli.user.login.entity.CodeEmailEntity;
import com.infodeli.user.login.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service("emailService")
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String from;

    @Override
    public void sendCodeEmail(CodeEmailEntity codeEmailEntity) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        System.out.println("发送方："+from);
        message.setTo(codeEmailEntity.getTo());
        message.setSubject(codeEmailEntity.getSubject());
        message.setText(getMessageText(codeEmailEntity));
        javaMailSender.send(message);
    }

    private String getMessageText(CodeEmailEntity codeEmailEntity){
        return codeEmailEntity.getContent()+codeEmailEntity.getCode()+"。（十分钟内有效）";
    }
}
