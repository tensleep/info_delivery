package com.infodeli.user.login.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.infodeli.common.exception.ExceptionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.infodeli.user.login.entity.UserLocalAuthEntity;
import com.infodeli.user.login.service.UserLocalAuthService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;



/**
 * 本地认证表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-29 13:33:05
 */
@RestController
@RequestMapping("user/local/auth")
public class UserLocalAuthController {
    @Autowired
    private UserLocalAuthService userLocalAuthService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("login:userlocalauth:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = userLocalAuthService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息：根据用户id查看认证【用户占领】
     */
    @RequestMapping("/info/{uid}")
    //@RequiresPermissions("login:userlocalauth:info")
    public R info(@PathVariable("uid") Long uid){
		List<UserLocalAuthEntity> list = userLocalAuthService.listByUid(uid);
        return R.ok().put("list", list);
    }

    /**
     * 认证：获取验证码，手机、邮箱认证方式【用户占领】
     */
    @PostMapping("/verify")
    //@RequiresPermissions("login:userlocalauth:save")
    public R verify(@RequestBody UserLocalAuthEntity userLocalAuth){
		userLocalAuthService.getVerifyCode(userLocalAuth);

        return R.ok();
    }

    /**
     * 保存：绑定 or 解绑，如果验证码通过的话【用户占领】
     */
    @PostMapping("/bind")
    //@RequiresPermissions("login:userlocalauth:save")
    public R bindOrUnbind(@RequestBody UserLocalAuthEntity userLocalAuth){
        userLocalAuthService.bindOrUnbind(userLocalAuth);
        if(userLocalAuth.getShowStatus() == 0){ // 解绑
            return R.res(ExceptionEnum.LOCAL_AUTH_UNBIND);
        }else if(userLocalAuth.getShowStatus() == 1){ // 绑定
            return R.res(ExceptionEnum.LOCAL_AUTH_BIND);
        }
        return R.res(ExceptionEnum.ALL_ERROR);
    }

    /**
     * 认证：同样是需要 检查验证码，但是不是为了 绑定or解绑，
     *      只是为了验证用户的身份。
     */
    @PostMapping("/auth")
    public R auth(@RequestBody UserLocalAuthEntity userLocalAuth){
        userLocalAuthService.auth(userLocalAuth);
        return R.res(ExceptionEnum.LOCAL_AUTH_SUCCESS);
    }


    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("login:userlocalauth:update")
    public R update(@RequestBody UserLocalAuthEntity userLocalAuth){
		userLocalAuthService.updateById(userLocalAuth);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("login:userlocalauth:delete")
    public R delete(@RequestBody Long[] authIds){
		userLocalAuthService.removeByIds(Arrays.asList(authIds));

        return R.ok();
    }

}
