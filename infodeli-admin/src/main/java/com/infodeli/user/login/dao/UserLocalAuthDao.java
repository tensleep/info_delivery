package com.infodeli.user.login.dao;

import com.infodeli.user.login.entity.UserLocalAuthEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 本地认证表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-29 13:33:05
 */
@Mapper
public interface UserLocalAuthDao extends BaseMapper<UserLocalAuthEntity> {
	
}
