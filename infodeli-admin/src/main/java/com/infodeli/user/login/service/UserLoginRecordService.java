package com.infodeli.user.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.user.login.entity.UserLoginRecordEntity;

import java.util.Map;

/**
 * 用户登录记录表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-29 13:33:05
 */
public interface UserLoginRecordService extends IService<UserLoginRecordEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

