package com.infodeli.user.login.dao;

import com.infodeli.user.login.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-12 19:36:06
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
