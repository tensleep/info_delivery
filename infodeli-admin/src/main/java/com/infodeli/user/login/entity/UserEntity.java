package com.infodeli.user.login.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-12 19:36:06
 */
@Data
@TableName("user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户id
	 */
	@TableId
	private Long uid;
	/**
	 * 学号
	 */
	private String stuid;
	/**
	 * 密码md5加密
	 */
	private String password;
	/**
	 * 昵称
	 */
	private String nickName;
	/**
	 * 是否正常[0-否,1是]
	 */
	private Integer userStatus;
	/**
	 * 头像
	 */
	private String portrait;
	/**
	 * 注册时间
	 */
	private String registerTime;
	/**
	 * 解封时间
	 */
	private String freeTime;

}
