package com.infodeli.user.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.user.login.entity.UserLocalAuthEntity;

import java.util.List;
import java.util.Map;

/**
 * 本地认证表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-29 13:33:05
 */
public interface UserLocalAuthService extends IService<UserLocalAuthEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<UserLocalAuthEntity> listByUid(Long uid);

    void getVerifyCode(UserLocalAuthEntity userLocalAuth);

    void bindOrUnbind(UserLocalAuthEntity userLocalAuth);

    void auth(UserLocalAuthEntity userLocalAuth);
}

