package com.infodeli.user.login.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户登录记录表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-29 13:33:05
 */
@Data
@TableName("user_login_record")
public class UserLoginRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 登录id
	 */
	@TableId
	private Long id;
	/**
	 * 用户id
	 */
	private Long uid;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * ip
	 */
	private String ip;
	/**
	 * 编号
	 */
	private String code;
	/**
	 * 消息
	 */
	private String message;
	/**
	 * 登录时间
	 */
	private String createTime;
	/**
	 * 是否显示
	 */
	private Integer showStatus;

}
