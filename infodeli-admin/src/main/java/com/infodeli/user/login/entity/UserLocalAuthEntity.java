package com.infodeli.user.login.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 本地认证表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-29 13:33:05
 */
@Data
@TableName("user_local_auth")
public class UserLocalAuthEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 认证id
	 */
	@TableId
	private Long authId;
	/**
	 * 用户id
	 */
	private Long uid;
	/**
	 * 类型[1-手机,2-邮箱]
	 */
	private Integer authType;
	/**
	 * 手机邮箱
	 */
	private String authContent;
	/**
	 * 是否显示
	 */
	private Integer showStatus;
	/**
	 * 验证码
	 */
	private String veriCode;
	/**
	 * 验证码超时时间
	 */
	private String overTime;

}
