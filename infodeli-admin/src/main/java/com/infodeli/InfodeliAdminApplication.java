package com.infodeli;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan({"com.infodeli.admin.dao","com.infodeli.user.login.dao","com.infodeli.user.dialog.dao"})
@SpringBootApplication
public class InfodeliAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfodeliAdminApplication.class, args);
	}

}
