package com.infodeli.component.controller;


import com.infodeli.common.utils.ComTool;
import com.infodeli.common.utils.JWTUtils;
import com.infodeli.common.utils.R;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@RestController
public class FileController {

    //private String host = "http://localhost:8081"; // 不可以，cros报错
    private String host = "http://localhost:88/api/user/";
    private String localFilePath = "F:/test_pic/"; // 本地文件保存路径
    private String localAvatarPath = "F:/test_pic/portrait/"; // 本地文件保存路径

    @PostMapping("/file")
    public R file(){
        return R.ok();
    }

    @PostMapping("/fileUpload")
    public R fileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws UnsupportedEncodingException {
        Long uid = Long.parseLong(JWTUtils.getTokenInfo(request.getHeader("token"), "uid"));

        if( file.isEmpty()){
            System.out.println("文件为空");
            return R.ok().put("error", "文件不能为空");
        }
        String fileName = file.getOriginalFilename();  // 文件名
        fileName = uid+ "_" + fileName;
        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
        //String filePath = "F:/test_pic/"; // 上传后的路径
        String fileFullPath = localFilePath +fileName;
        System.out.println("["+ ComTool.getDateStr()+"]  文件完整路径"+fileFullPath);
        File dest = new File(fileFullPath);
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.ok().put("fileUrl", host + "test_pic/"+fileName);
    }

    @PostMapping("upload/avatar")
    public R uploadAvatar(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws UnsupportedEncodingException {
        Long uid = Long.parseLong(JWTUtils.getTokenInfo(request.getHeader("token"), "uid"));

        if( file.isEmpty()){
            System.out.println("文件为空");
            return R.ok().put("error", "文件不能为空");
        }
        String fileName = file.getOriginalFilename();  // 文件名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
        fileName = uid + "_portrait" + ComTool.getSimpleDateStr() + suffixName;
        //String filePath = "F:/test_pic/"; // 上传后的路径
        String fileFullPath = localAvatarPath +fileName;
        System.out.println("["+ ComTool.getDateStr()+"]  文件完整路径"+fileFullPath);
        File dest = new File(fileFullPath);
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.ok().put("fileUrl", host + "test_pic/portrait/"+fileName);
    }
}