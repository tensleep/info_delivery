package com.infodeli.component.bean;

import lombok.Data;

@Data
public class Message {
    private Long dialogId;
    private Long sendUid;
    private Long recvUid;
    private String content;
}
