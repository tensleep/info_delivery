package com.infodeli.component.handler;

import javax.websocket.Session;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class WebSocketMap {

    private static Map<Long , Session> map = new ConcurrentHashMap<>();

    public static Map<Long , Session> getWebsocketMap(){
        return map;
    }
}
