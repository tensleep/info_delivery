package com.infodeli.component.handler;

import com.infodeli.common.exception.LoginException;
import com.infodeli.common.exception.ServiceException;
import com.infodeli.common.utils.R;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 统一异常处理
 */
@RestControllerAdvice
public class MyExceptionHandler {
    @ExceptionHandler({LoginException.class})
    public R handlerLoginException(LoginException e){
        return R.error(e.getCode(),e.getMsg());
    }

    @ExceptionHandler({ServiceException.class})
    public R handlerServiceException(ServiceException e){
        return R.error(e.getCode(),e.getMsg());
    }
}
