package com.infodeli.component.config;

//import com.infodeli.component.listener.WebSocketListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

@Configuration
public class WebSocketConfig /*extends ServerEndpointConfig.Configurator*/ {

    // 修改握手，在握手协议建立之前 修改其中携带的内容
    //@Override
    //public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
    //    HttpSession httpSession = (HttpSession)request.getHttpSession();
    //    if(httpSession != null){
    //        // 将 httpSession 保存起来
    //        sec.getUserProperties().put(HttpSession.class.getName(), httpSession);
    //    }
    //    super.modifyHandshake(sec, request, response);
    //}

    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        return new ServerEndpointExporter();
    }


    //@Autowired
    //WebSocketListener webSocketListener;

    //将 WebSocketListener 加入到容器中

    //@Bean
    //public ServletListenerRegistrationBean<WebSocketListener> servletListenerRegistrationBean(){
    //    ServletListenerRegistrationBean<WebSocketListener> servletListenerRegistrationBean
    //        = new ServletListenerRegistrationBean<>();
    //    servletListenerRegistrationBean.setListener(webSocketListener);
    //    return servletListenerRegistrationBean;
    //}

}
