package com.infodeli.admin.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.infodeli.admin.entity.InfoAttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.infodeli.admin.vo.InfoAttrVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@Mapper
public interface InfoAttrDao extends BaseMapper<InfoAttrEntity> {
    IPage<InfoAttrVo> queryPageVo(IPage<?> page, @Param("ew") Wrapper<InfoAttrVo> queryWrapper);
}
