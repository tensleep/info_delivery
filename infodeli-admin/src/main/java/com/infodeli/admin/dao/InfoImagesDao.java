package com.infodeli.admin.dao;

import com.infodeli.admin.entity.InfoImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 信息图片
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@Mapper
public interface InfoImagesDao extends BaseMapper<InfoImagesEntity> {
	
}
