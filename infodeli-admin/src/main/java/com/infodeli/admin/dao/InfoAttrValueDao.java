package com.infodeli.admin.dao;

import com.infodeli.admin.entity.InfoAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 信息属性值
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@Mapper
public interface InfoAttrValueDao extends BaseMapper<InfoAttrValueEntity> {
	
}
