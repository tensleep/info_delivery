package com.infodeli.admin.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.infodeli.admin.entity.InfoCommentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.infodeli.admin.entity.InfoCommentReplyEntity;
import com.infodeli.admin.service.InfoService;
import com.infodeli.admin.vo.InfoCommentVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 信息评论表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-10 18:06:20
 */
@Mapper
public interface InfoCommentDao extends BaseMapper<InfoCommentEntity> {

    IPage<InfoCommentVo> queryPageByInfoId(IPage<?> page, @Param("infoId") Long infoId);

    IPage<InfoCommentVo> queryPageVo(IPage<?> page, @Param("ew") Wrapper<InfoCommentVo> queryWrapper);

    void addReplyNum(Long commentId, Integer num);

    Integer deleteLogic(@Param("ew") Wrapper<InfoCommentEntity> queryWrapper);
	
}
