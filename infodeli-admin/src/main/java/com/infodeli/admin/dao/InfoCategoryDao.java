package com.infodeli.admin.dao;

import com.infodeli.admin.entity.InfoCategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@Mapper
public interface InfoCategoryDao extends BaseMapper<InfoCategoryEntity> {
	
}
