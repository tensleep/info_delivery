package com.infodeli.admin.dao;

import com.infodeli.admin.entity.InfoAttrSelectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性可选值表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-28 18:39:24
 */
@Mapper
public interface InfoAttrSelectDao extends BaseMapper<InfoAttrSelectEntity> {
	
}
