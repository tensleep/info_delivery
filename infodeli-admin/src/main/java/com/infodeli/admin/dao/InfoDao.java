package com.infodeli.admin.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.infodeli.admin.entity.InfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.infodeli.admin.vo.InfoCommentVo;
import com.infodeli.admin.vo.InfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 信息
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@Mapper
public interface InfoDao extends BaseMapper<InfoEntity> {

    void addCommentNum(Long infoId, Integer num);

    List<InfoVo> queryPageVo(@Param("ew") Wrapper<InfoEntity> queryWrapper);

    InfoVo getVoForUpdate(@Param("ew") Wrapper<InfoEntity> queryWrapper);
	
}
