package com.infodeli.admin.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.infodeli.admin.entity.InfoCommentReplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.infodeli.admin.vo.InfoCommentReplyVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 信息评论回复表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-10 18:06:20
 */
@Mapper
public interface InfoCommentReplyDao extends BaseMapper<InfoCommentReplyEntity> {

    IPage<InfoCommentReplyVo> queryPageVo(IPage<?> page, @Param("ew") Wrapper<InfoCommentReplyVo> queryWrapper);

    Integer deleteLogic(@Param("ew") Wrapper<InfoCommentReplyEntity> queryWrapper);
}
