package com.infodeli.admin.dao;

import com.infodeli.admin.entity.InfoInputTypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 输入种类表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-28 18:39:24
 */
@Mapper
public interface InfoInputTypeDao extends BaseMapper<InfoInputTypeEntity> {
	
}
