package com.infodeli.admin.controller;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.infodeli.admin.vo.InfoAttrVo;
import com.infodeli.admin.vo.InfoVo;
import com.infodeli.common.utils.JWTUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.infodeli.admin.entity.InfoEntity;
import com.infodeli.admin.service.InfoService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;

import javax.servlet.http.HttpServletRequest;
import javax.sound.sampled.Line;


/**
 * 信息
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@RestController
@RequestMapping("info")
public class InfoController {
    @Autowired
    private InfoService infoService;

    /**
     * 列表：分页查询 信息 【占领】
     */
    @RequestMapping("/list")
    //@RequiresPermissions("admin:info:list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = infoService.queryPageVo(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/list_test")
    //@RequiresPermissions("admin:info:list")
    public R listTest(@RequestParam Map<String, Object> params){
        PageUtils page = infoService.queryPage1(params);

        return R.ok().put("page", page);
    }

    /**
     * 信息审核：更新showStatus
     */
    @RequestMapping("/update/status")
    public R updateStatus(@RequestBody List<InfoVo> infoVos){
        infoService.updateInfoStatus(infoVos);
        return R.ok();
    }

    /**
     * 信息：获取一个具体的信息
     */
    @RequestMapping("/info/{infoId}")
    //@RequiresPermissions("admin:info:info")
    public R info(@PathVariable("infoId") Long infoId){
		InfoVo info = infoService.getVoById(infoId);

        return R.ok().put("info", info);
    }

    /**
     * 保存：保存用户上传的信息【占领】
     */
    @RequestMapping("/user/save")
    public R userSave(@RequestBody InfoVo vo, HttpServletRequest request) throws UnsupportedEncodingException {
        Long uid = Long.parseLong(JWTUtils.getTokenInfo(request.getHeader("token"), "uid"));
        infoService.userSave(vo, uid);
        return R.ok();
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("admin:info:save")
    public R save(@RequestBody InfoEntity info){
		infoService.save(info);

        return R.ok();
    }

    /**
     * 修改：用户修改 信息内容 【占领】
     */
    @RequestMapping("/update")
    //@RequiresPermissions("admin:info:update")
    public R update(@RequestBody InfoVo info){
		infoService.updateVo(info);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("admin:info:delete")
    public R delete(@RequestBody Long[] infoIds){
		infoService.removeByIds(Arrays.asList(infoIds));

        return R.ok();
    }

}
