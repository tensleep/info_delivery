package com.infodeli.admin.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.infodeli.admin.entity.InfoImagesEntity;
import com.infodeli.admin.service.InfoImagesService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;



/**
 * 信息图片
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@RestController
@RequestMapping("info/images")
public class InfoImagesController {
    @Autowired
    private InfoImagesService infoImagesService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("admin:infoimages:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = infoImagesService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{imgId}")
    //@RequiresPermissions("admin:infoimages:info")
    public R info(@PathVariable("imgId") Long imgId){
		InfoImagesEntity infoImages = infoImagesService.getById(imgId);

        return R.ok().put("infoImages", infoImages);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("admin:infoimages:save")
    public R save(@RequestBody InfoImagesEntity infoImages){
		infoImagesService.save(infoImages);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("admin:infoimages:update")
    public R update(@RequestBody InfoImagesEntity infoImages){
		infoImagesService.updateById(infoImages);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("admin:infoimages:delete")
    public R delete(@RequestBody Long[] imgIds){
		infoImagesService.removeByIds(Arrays.asList(imgIds));

        return R.ok();
    }

}
