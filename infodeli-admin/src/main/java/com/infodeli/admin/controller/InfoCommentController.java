package com.infodeli.admin.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.infodeli.admin.entity.InfoCommentEntity;
import com.infodeli.admin.service.InfoCommentService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;



/**
 * 信息评论表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-10 18:06:20
 */
@RestController
@RequestMapping("info/comment")
public class InfoCommentController {
    @Autowired
    private InfoCommentService infoCommentService;

    /**
     * 列表：分页 查找 消息的评论 【占领】
     */
    @RequestMapping("/list/{infoId}")
    //@RequiresPermissions("admin:infocomment:list")
    public R list(@RequestBody Map<String, Object> params, @PathVariable("infoId")Long infoId){
        //PageUtils page = infoCommentService.queryPage(params);
        PageUtils page = infoCommentService.queryPageByInfoId(params, infoId);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{commentId}")
    //@RequiresPermissions("admin:infocomment:info")
    public R info(@PathVariable("commentId") Long commentId){
		InfoCommentEntity infoComment = infoCommentService.getById(commentId);

        return R.ok().put("infoComment", infoComment);
    }

    /**
     * 保存：对 消息 进行 评论【占领】
     */
    @RequestMapping("/save")
    //@RequiresPermissions("admin:infocomment:save")
    public R save(@RequestBody InfoCommentEntity infoComment){
		//infoCommentService.save(infoComment);
        infoCommentService.comment(infoComment);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("admin:infocomment:update")
    public R update(@RequestBody InfoCommentEntity infoComment){
		infoCommentService.updateById(infoComment);

        return R.ok();
    }

    /**
     * 删除：需要传入 调用service 的三个参数【占领】
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("admin:infocomment:delete")
    public R delete(@RequestBody InfoCommentEntity entity){
		//infoCommentService.removeByIds(Arrays.asList(commentIds));
        infoCommentService.deleteLogic(entity.getCommentId(), entity.getUid(), entity.getInfoId());
        return R.ok();
    }

}
