package com.infodeli.admin.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.infodeli.admin.entity.InfoCategoryEntity;
import com.infodeli.admin.service.InfoCategoryService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;



/**
 * 商品三级分类
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@RestController
@RequestMapping("info/category")
public class InfoCategoryController {
    @Autowired
    private InfoCategoryService infoCategoryService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("admin:infocategory:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = infoCategoryService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 获取所有的 信息种类（属性结构）
     * @return
     */
    @GetMapping("/list/tree")
    public R listTree(){
        List<InfoCategoryEntity> data = infoCategoryService.queryListTree();
        return R.ok().put("data", data);
    }


    /**
     * 信息：根据 种类id 来获取某个
     */
    @RequestMapping("/info/{catId}")
    //@RequiresPermissions("admin:infocategory:info")
    public R info(@PathVariable("catId") Long catId){
		InfoCategoryEntity infoCategory = infoCategoryService.getById(catId);

        return R.ok().put("infoCategory", infoCategory);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("admin:infocategory:save")
    public R save(@RequestBody InfoCategoryEntity infoCategory){
		infoCategoryService.save(infoCategory);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("admin:infocategory:update")
    public R update(@RequestBody InfoCategoryEntity infoCategory){
		infoCategoryService.updateById(infoCategory);

        return R.ok();
    }

    /**
     * 修改：admin，修改 种类之间的顺序
     */
    @RequestMapping("/update/sort")
    //@RequiresPermissions("admin:infocategory:update")
    public R updateSort(@RequestBody InfoCategoryEntity[] infoCategory){
        //infoCategoryService.updateById(infoCategory);
        infoCategoryService.updateBatchById(Arrays.asList(infoCategory));
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("admin:infocategory:delete")
    public R delete(@RequestBody Long[] catIds){
		//infoCategoryService.removeByIds(Arrays.asList(catIds));

        infoCategoryService.removeByIdsSelf(Arrays.asList(catIds));

        return R.ok();
    }

}
