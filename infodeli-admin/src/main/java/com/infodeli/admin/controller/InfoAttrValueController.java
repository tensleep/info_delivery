package com.infodeli.admin.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.infodeli.admin.entity.InfoAttrValueEntity;
import com.infodeli.admin.service.InfoAttrValueService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;



/**
 * 信息属性值
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@RestController
@RequestMapping("info/attr/value")
public class InfoAttrValueController {
    @Autowired
    private InfoAttrValueService infoAttrValueService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("admin:infoattrvalue:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = infoAttrValueService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{valId}")
    //@RequiresPermissions("admin:infoattrvalue:info")
    public R info(@PathVariable("valId") Long valId){
		InfoAttrValueEntity infoAttrValue = infoAttrValueService.getById(valId);

        return R.ok().put("infoAttrValue", infoAttrValue);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("admin:infoattrvalue:save")
    public R save(@RequestBody InfoAttrValueEntity infoAttrValue){
		infoAttrValueService.save(infoAttrValue);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("admin:infoattrvalue:update")
    public R update(@RequestBody InfoAttrValueEntity infoAttrValue){
		infoAttrValueService.updateById(infoAttrValue);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("admin:infoattrvalue:delete")
    public R delete(@RequestBody Long[] valIds){
		infoAttrValueService.removeByIds(Arrays.asList(valIds));

        return R.ok();
    }

}
