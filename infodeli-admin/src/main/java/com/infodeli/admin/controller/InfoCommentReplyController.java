package com.infodeli.admin.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.infodeli.admin.entity.InfoCommentReplyEntity;
import com.infodeli.admin.service.InfoCommentReplyService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;



/**
 * 信息评论回复表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-10 18:06:20
 */
@RestController
@RequestMapping("info/comment/reply")
public class InfoCommentReplyController {
    @Autowired
    private InfoCommentReplyService infoCommentReplyService;

    /**
     * 列表：分页 获取某个评论的 回复【占领】
     */
    @RequestMapping("/list/{commentId}")
    //@RequiresPermissions("admin:infocommentreply:list")
    public R list(@RequestBody Map<String, Object> params, @PathVariable("commentId") Long commentId){
        //PageUtils page = infoCommentReplyService.queryPage(params);
        PageUtils page = infoCommentReplyService.queryPageByCommentId(params, commentId);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{replyId}")
    //@RequiresPermissions("admin:infocommentreply:info")
    public R info(@PathVariable("replyId") Long replyId){
		InfoCommentReplyEntity infoCommentReply = infoCommentReplyService.getById(replyId);

        return R.ok().put("infoCommentReply", infoCommentReply);
    }

    /**
     * 保存：对 评论进行 回复【占领】
     */
    @RequestMapping("/save")
    //@RequiresPermissions("admin:infocommentreply:save")
    public R save(@RequestBody InfoCommentReplyEntity infoCommentReply){
		//infoCommentReplyService.save(infoCommentReply);
        infoCommentReplyService.reply(infoCommentReply);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("admin:infocommentreply:update")
    public R update(@RequestBody InfoCommentReplyEntity infoCommentReply){
		infoCommentReplyService.updateById(infoCommentReply);

        return R.ok();
    }

    /**
     * 删除：需要传入 调用 service 的四个参数，其中fromId是删除的 发起者【占领】
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("admin:infocommentreply:delete")
    public R delete(@RequestBody InfoCommentReplyEntity entity){
		//infoCommentReplyService.removeByIds(Arrays.asList(replyIds));
        infoCommentReplyService.deleteLogic(entity.getReplyId(), entity.getFromId(), entity.getInfoId(), entity.getCommentId());
        return R.ok();
    }

}
