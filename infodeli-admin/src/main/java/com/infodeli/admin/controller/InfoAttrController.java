package com.infodeli.admin.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.infodeli.admin.service.InfoCategoryService;
import com.infodeli.admin.vo.InfoAttrVo;
import com.infodeli.common.utils.MyMapUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.infodeli.admin.entity.InfoAttrEntity;
import com.infodeli.admin.service.InfoAttrService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;



/**
 * 
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@RestController
@Slf4j
@RequestMapping("info/attr")
public class InfoAttrController {
    @Autowired
    private InfoAttrService infoAttrService;

    @Autowired
    private InfoCategoryService infoCategoryService;

    /**
     * 列表：分页查询所有
     */
    @RequestMapping("/list")
    //@RequiresPermissions("admin:infoattr:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = infoAttrService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 【分页】某个信息种类 的 所有属性 【占领】老
     */
    @RequestMapping("/list/{catId}")
    //@RequiresPermissions("admin:infoattr:list")
    public R list(@RequestBody Map<String, Object> params, @PathVariable("catId") Long catId){
        // 因为默认是直接分页查询的，没有where过滤，所以我们要自己编写针对 某个信息种类 的属性查询
        PageUtils page = infoAttrService.queryPageByCatId(params, catId);
        return R.ok().put("page", page);
    }

    /**
     * 【分页】某个信息种类 的 所有属性 【占领】新
     */
    @PostMapping("/list/vo/{catId}")
    public R listVo(@RequestBody Map<String, Object> params, @PathVariable("catId") Long catId){
        PageUtils page = infoAttrService.queryPageVoByCatId(params, catId);
        List<Long> catIdPath = infoCategoryService.findCatIdPath(catId);
        return R.ok().put("page",page).put("catIdPath", catIdPath);
    }


    /**
     * 某个信息种类 的 所有属性【用户用户上传】
     */
    @RequestMapping("/{catId}")
    public R infoUpStep02(@PathVariable("catId") Long catId){
        List<InfoAttrVo> infoAttrVos = infoAttrService.listByCatId(catId);
        return R.ok().put( "info_attr", infoAttrVos);
    }





    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
    //@RequiresPermissions("admin:infoattr:info")
    public R info(@PathVariable("attrId") Long attrId){
		InfoAttrEntity infoAttr = infoAttrService.getById(attrId);

		// 填充 其中的 catIdPath 属性
        //Long catId = infoAttr.getCatId();
        //List<Long>  catIdPath = infoCategoryService.findCatIdPath(catId);
        //infoAttr.setCatIdPath(catIdPath);

        return R.ok().put("infoAttr", infoAttr);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("admin:infoattr:save")
    public R save(@RequestBody InfoAttrVo infoAttrVo){
		infoAttrService.save(infoAttrVo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("admin:infoattr:update")
    public R update(@RequestBody InfoAttrEntity infoAttr){
		infoAttrService.updateById(infoAttr);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update/{attrId}/{showStatus}")
    //@RequiresPermissions("admin:infoattr:update")
    public R update(@PathVariable("attrId")Long attrId, @PathVariable("showStatus")Integer showStatus){
        infoAttrService.updateShowStatus(attrId, showStatus);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("admin:infoattr:delete")
    public R delete(@RequestBody Long[] attrIds){
		infoAttrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
