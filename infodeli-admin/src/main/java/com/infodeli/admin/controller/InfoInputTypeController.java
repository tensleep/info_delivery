package com.infodeli.admin.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.infodeli.admin.entity.InfoInputTypeEntity;
import com.infodeli.admin.service.InfoInputTypeService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;



/**
 * 输入种类表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-28 18:39:24
 */
@RestController
@RequestMapping("info/input/type")
public class InfoInputTypeController {
    @Autowired
    private InfoInputTypeService infoInputTypeService;

    /**
     * 列表：进行基本的查询，不能按 关键词 查【简单占领】
     * 经过修改，可以完成分页、关键词【占领】
     */
    @RequestMapping("/list")
    //@RequiresPermissions("admin:infoinputtype:list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = infoInputTypeService.queryPage(params);

        return R.ok().put("page", page);
    }
    /**
     * 列表：不不不分页、关键词、showStatus==1【简单占领】
     */
    @RequestMapping("/list/all/show")
    //@RequiresPermissions("admin:infoinputtype:list")
    public R listAllShow(){
        List<InfoInputTypeEntity> list = infoInputTypeService.listAllShow();


        return R.ok().put("list", list);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{typeId}")
    //@RequiresPermissions("admin:infoinputtype:info")
    public R info(@PathVariable("typeId") Long typeId){
		InfoInputTypeEntity infoInputType = infoInputTypeService.getById(typeId);

        return R.ok().put("infoInputType", infoInputType);
    }

    /**
     * 保存：新增一条输入类型【占领】
     */
    @RequestMapping("/save")
    //@RequiresPermissions("admin:infoinputtype:save")
    public R save(@RequestBody InfoInputTypeEntity infoInputType){
		infoInputTypeService.save(infoInputType);

        return R.ok();
    }

    /**
     * 修改：修改某条记录【占领】
     */
    @RequestMapping("/update")
    //@RequiresPermissions("admin:infoinputtype:update")
    public R update(@RequestBody InfoInputTypeEntity infoInputType){
		infoInputTypeService.updateById(infoInputType);

        return R.ok();
    }

    // 修改 showStatus【占领】
    @PostMapping("/update/{typeId}/{showStatus}")
    public R updateShowStatus(@PathVariable("typeId")Long typeId, @PathVariable("showStatus")Integer showStatus){
        infoInputTypeService.updateShowStatus(typeId, showStatus);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("admin:infoinputtype:delete")
    public R delete(@RequestBody Long[] typeIds){
		infoInputTypeService.removeByIds(Arrays.asList(typeIds));

        return R.ok();
    }

}
