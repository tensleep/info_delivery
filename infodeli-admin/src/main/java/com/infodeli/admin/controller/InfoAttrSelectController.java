package com.infodeli.admin.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.infodeli.admin.entity.InfoAttrSelectEntity;
import com.infodeli.admin.service.InfoAttrSelectService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.R;



/**
 * 属性可选值表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-28 18:39:24
 */
@RestController
@RequestMapping("info/attr/select")
public class InfoAttrSelectController {
    @Autowired
    private InfoAttrSelectService infoAttrSelectService;

    /**
     * 列表：所有的selectValue 【简单占领】
     */
    @RequestMapping("/list/{attrId}")
    //@RequiresPermissions("admin:infoattrselect:list")
    public R list(@RequestBody Map<String, Object> params, @PathVariable("attrId")Long attrId){
        PageUtils page = infoAttrSelectService.queryPageByAttrId(params, attrId);

        return R.ok().put("page", page);
    }

    /**
     * 列表：所有的selectValue，且 showStatus == 1 【再说】
     */


    /**
     * 信息
     */
    @RequestMapping("/info/{selectId}")
    //@RequiresPermissions("admin:infoattrselect:info")
    public R info(@PathVariable("selectId") Long selectId){
		InfoAttrSelectEntity infoAttrSelect = infoAttrSelectService.getById(selectId);

        return R.ok().put("infoAttrSelect", infoAttrSelect);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("admin:infoattrselect:save")
    public R save(@RequestBody InfoAttrSelectEntity infoAttrSelect){
		infoAttrSelectService.save(infoAttrSelect);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("admin:infoattrselect:update")
    public R update(@RequestBody InfoAttrSelectEntity infoAttrSelect){
		infoAttrSelectService.updateById(infoAttrSelect);

        return R.ok();
    }

    /**
     * 修改：修改showStatus【简单占领】
     */
    @RequestMapping("/update/{selectId}/{showStatus}")
    //@RequiresPermissions("admin:infoattrselect:update")
    public R update(@PathVariable("selectId") Long selectId, @PathVariable("showStatus")Integer showStatus){
        infoAttrSelectService.updateShowStatus(selectId, showStatus);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("admin:infoattrselect:delete")
    public R delete(@RequestBody Long[] selectIds){
		infoAttrSelectService.removeByIds(Arrays.asList(selectIds));

        return R.ok();
    }

}
