package com.infodeli.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.admin.vo.InfoAttrVo;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.admin.entity.InfoAttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
public interface InfoAttrService extends IService<InfoAttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageVoByCatId(Map<String, Object> params, Long catId);

    PageUtils queryPageByCatId(Map<String, Object> params, Long catId);

    List<InfoAttrVo> listByCatId(Long catId);

    void save(InfoAttrVo infoAttrVo);

    void updateShowStatus(Long attrId, Integer showStatus);
}

