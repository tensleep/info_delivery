package com.infodeli.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.admin.entity.InfoAttrSelectEntity;

import java.util.List;
import java.util.Map;

/**
 * 属性可选值表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-28 18:39:24
 */
public interface InfoAttrSelectService extends IService<InfoAttrSelectEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<InfoAttrSelectEntity> listByAttrId(List<Long> attrIds);

    PageUtils queryPageByAttrId(Map<String, Object> params, Long attrId);

    void updateShowStatus(Long selectId, Integer showStatus);
}

