package com.infodeli.admin.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.Query;

import com.infodeli.admin.dao.InfoAttrValueDao;
import com.infodeli.admin.entity.InfoAttrValueEntity;
import com.infodeli.admin.service.InfoAttrValueService;


@Service("infoAttrValueService")
public class InfoAttrValueServiceImpl extends ServiceImpl<InfoAttrValueDao, InfoAttrValueEntity> implements InfoAttrValueService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InfoAttrValueEntity> page = this.page(
                new Query<InfoAttrValueEntity>().getPage(params),
                new QueryWrapper<InfoAttrValueEntity>()
        );

        return new PageUtils(page);
    }

}