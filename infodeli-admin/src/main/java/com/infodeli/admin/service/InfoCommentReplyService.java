package com.infodeli.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.admin.entity.InfoCommentReplyEntity;

import java.util.Map;

/**
 * 信息评论回复表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-10 18:06:20
 */
public interface InfoCommentReplyService extends IService<InfoCommentReplyEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByCommentId(Map<String, Object> params, Long commentId);

    void reply(InfoCommentReplyEntity entity);

    void deleteLogic(Long replyId, Long uid, Long infoId, Long commentId);

}

