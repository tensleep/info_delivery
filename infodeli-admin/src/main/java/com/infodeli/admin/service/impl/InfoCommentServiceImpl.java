package com.infodeli.admin.service.impl;

import com.infodeli.admin.entity.InfoCommentReplyEntity;
import com.infodeli.admin.entity.InfoEntity;
import com.infodeli.admin.service.InfoCommentReplyService;
import com.infodeli.admin.service.InfoService;
import com.infodeli.admin.vo.InfoCommentVo;
import com.infodeli.common.utils.ComTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.Query;

import com.infodeli.admin.dao.InfoCommentDao;
import com.infodeli.admin.entity.InfoCommentEntity;
import com.infodeli.admin.service.InfoCommentService;


@Service("infoCommentService")
public class InfoCommentServiceImpl extends ServiceImpl<InfoCommentDao, InfoCommentEntity> implements InfoCommentService {

    @Autowired
    InfoService infoService;

    @Autowired
    InfoCommentReplyService infoCommentReplyService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InfoCommentEntity> page = this.page(
                new Query<InfoCommentEntity>().getPage(params),
                new QueryWrapper<InfoCommentEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 分页查找评论，成功实现分页
     * @param params
     * @param infoId
     * @return
     */
    @Override
    public PageUtils queryPageByInfoId(Map<String, Object> params, Long infoId) {
        // 方式1：直接使用明确的查询条件，即写在sql语句中
        //IPage<InfoCommentVo> infoCommentVos = baseMapper.queryPageByInfoId(new Query<InfoCommentVo>().getPage(params), infoId);
        // 方式2：使用QueryWrapper，即代码的形式
        QueryWrapper<InfoCommentVo> wrapper = new QueryWrapper<>();
        wrapper.eq("info_id", infoId).eq("show_status", 1);
        IPage<InfoCommentVo> infoCommentVos = baseMapper.queryPageVo(new Query<InfoCommentVo>().getPage(params), wrapper);

        PageUtils pageUtils = new PageUtils(infoCommentVos);
        //return infoCommentVos;
        return pageUtils;
    }

    /*
{
    infoId: ,
    uid: ,
    commentContent: ,
}
     */
    @Override // 评论 info
    public void comment(InfoCommentEntity entity){
        entity.setReplyNum(0);
        entity.setLikeNum(0);
        entity.setHateNum(0);
        entity.setCreateTime(ComTool.getDateStr());
        entity.setShowStatus(1);
        if(entity.getUid() == entity.getToId()){
            entity.setReadStatus(1);
        }else{
            entity.setReadStatus(0);
        }
        this.save(entity);
        // 增加 info 的 comment_num
        infoService.addCommentNum(entity.getInfoId(), 1);
    }

    @Override // 删除 comment
    public void deleteLogic(Long commentId, Long uid, Long infoId){
        InfoEntity byId = infoService.getById(infoId);
        QueryWrapper<InfoCommentEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("comment_id", commentId).eq("info_id",infoId);
        if(byId == null || byId.getUserId() != uid){
            wrapper.eq("uid", uid);
        }
        Integer num = this.baseMapper.deleteLogic(wrapper); // 执行删除
        if(num == null || num == 0){
            return;
        }
        // 对于底层的回复，不进行处理，因为上层 的 comment 已经无法查找到了，相当于已经找不到了。
        int count = infoCommentReplyService.count(new QueryWrapper<InfoCommentReplyEntity>().eq("comment_id", commentId).eq("show_status", 1));
        infoService.addCommentNum(infoId, -(1+count));
    }

    @Override // 修改 comment 中的 replyNum，同时更新 info 中的 commentNum 【正：增；负：减】
    public void addReplyNum(Long infoId, Long commentId, Integer num) {
        baseMapper.addReplyNum(commentId, num);
        infoService.addCommentNum(infoId, num);
    }

}