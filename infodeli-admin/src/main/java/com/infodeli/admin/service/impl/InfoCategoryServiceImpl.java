package com.infodeli.admin.service.impl;

import org.springframework.stereotype.Service;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.Query;

import com.infodeli.admin.dao.InfoCategoryDao;
import com.infodeli.admin.entity.InfoCategoryEntity;
import com.infodeli.admin.service.InfoCategoryService;


@Service("infoCategoryService")
public class InfoCategoryServiceImpl extends ServiceImpl<InfoCategoryDao, InfoCategoryEntity> implements InfoCategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InfoCategoryEntity> page = this.page(
                new Query<InfoCategoryEntity>().getPage(params),
                new QueryWrapper<InfoCategoryEntity>()
        );

        return new PageUtils(page);
    }

    // 返回 信息种类 的 树形集合
    @Override
    public List<InfoCategoryEntity> queryListTree() {
        List<InfoCategoryEntity> list = this.list();

        Map<Long, InfoCategoryEntity> map = new HashMap<>(list.size()/2);
        for(InfoCategoryEntity entity : list){
            if(entity.getCatLevel() < 3){
                map.put(entity.getCatId(), entity);
            }
        }
        for(InfoCategoryEntity entity : list){
            if(entity.getCatLevel() > 1){
                InfoCategoryEntity parent = map.get(entity.getParentCid());
                if(parent.getChildren() == null){
                    parent.setChildren(new ArrayList<>());
                }
                parent.getChildren().add(entity);
            }
        }
        list = new ArrayList<>();
        for(Map.Entry<Long, InfoCategoryEntity> entry : map.entrySet()){
            if(entry.getValue().getCatLevel() == 1){
                list.add(entry.getValue());
            }
        }
        sortInfoCategoryEntity(list);

        return list;

    }

    @Override
    public void removeByIdsSelf(List<Long> list) {
        //TODO 删除某个信息分类前，检查其是否被引用
        baseMapper.deleteBatchIds(list);
    }

    @Override // 查询一个 种类id 的完整id路径
    public List<Long> findCatIdPath(Long catId) {
        List<Long> path = new ArrayList<>();
        doFindCatIdPath(catId, path);
        //System.out.println("查询"+catId+"的路径为"+ path);
        return path;
    }

    public void doFindCatIdPath(Long catId, List<Long> path){
        InfoCategoryEntity entity = this.getById(catId);
        if(entity.getParentCid() != 0){
            doFindCatIdPath(entity.getParentCid(), path);
        }
        path.add(catId);
    }

    /**
     * 对 树形集合 按照sort字段 从小到大进行排序
     * @param list
     */
    public void sortInfoCategoryEntity(List<InfoCategoryEntity> list){
        Collections.sort(list, (e1, e2)->{
            return e1.getSort() - e2.getSort();
        });
        for(InfoCategoryEntity entity : list){
            if(entity.getChildren() != null && entity.getChildren().size()>0){
                sortInfoCategoryEntity(entity.getChildren());
            }
        }
    }


}