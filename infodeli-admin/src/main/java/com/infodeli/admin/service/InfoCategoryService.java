package com.infodeli.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.admin.entity.InfoCategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
public interface InfoCategoryService extends IService<InfoCategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<InfoCategoryEntity> queryListTree();

    void removeByIdsSelf(List<Long> list);

    List<Long> findCatIdPath(Long catId);
}

