package com.infodeli.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.admin.vo.InfoCommentVo;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.admin.entity.InfoCommentEntity;

import java.util.List;
import java.util.Map;

/**
 * 信息评论表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-10 18:06:20
 */
public interface InfoCommentService extends IService<InfoCommentEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByInfoId(Map<String, Object> params, Long infoId);

    void comment(InfoCommentEntity entity);

    void addReplyNum(Long infoId, Long commentId, Integer num);

    void deleteLogic(Long commentId, Long uid, Long infoId);

    
}

