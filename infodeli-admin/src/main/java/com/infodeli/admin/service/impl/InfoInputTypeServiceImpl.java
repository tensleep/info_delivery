package com.infodeli.admin.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.Query;

import com.infodeli.admin.dao.InfoInputTypeDao;
import com.infodeli.admin.entity.InfoInputTypeEntity;
import com.infodeli.admin.service.InfoInputTypeService;


@Service("infoInputTypeService")
public class InfoInputTypeServiceImpl extends ServiceImpl<InfoInputTypeDao, InfoInputTypeEntity> implements InfoInputTypeService {

    @Override // 分页、关键词
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<InfoInputTypeEntity> wrapper = new QueryWrapper<>();
        String typeName = (String) params.get("key");
        if(typeName != null && !"".equals(typeName)){
            wrapper.like("type_name",typeName);
        }
        IPage<InfoInputTypeEntity> page = this.page(
                new Query<InfoInputTypeEntity>().getPage(params),
                wrapper
        );
        return new PageUtils(page);
    }

    @Override // 分页、关键词、showStatus == 1
    public PageUtils queryPageShow(Map<String, Object> params) {
        QueryWrapper<InfoInputTypeEntity> wrapper = new QueryWrapper<>();
        String typeName = (String) params.get("key");
        wrapper.eq("show_status", 1);
        if(typeName != null && !"".equals(typeName)){
            wrapper.like("type_name",typeName);
        }
        IPage<InfoInputTypeEntity> page = this.page(
                new Query<InfoInputTypeEntity>().getPage(params),
                wrapper
        );
        return new PageUtils(page);
    }

    @Override // 不分页、showStatus == 1
    public List<InfoInputTypeEntity> listAllShow() {
        QueryWrapper<InfoInputTypeEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("show_status", 1);
        List<InfoInputTypeEntity> list = this.list(wrapper);
        return list;
    }

    @Override // 更新 输入类型 的 showStatus
    public void updateShowStatus(Long typeId, Integer showStatus) {
        InfoInputTypeEntity entity = new InfoInputTypeEntity();
        entity.setTypeId(typeId);
        entity.setShowStatus(showStatus);
        this.updateById(entity);
    }



}