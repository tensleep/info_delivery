package com.infodeli.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.admin.entity.InfoImagesEntity;

import java.util.Map;

/**
 * 信息图片
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
public interface InfoImagesService extends IService<InfoImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

