package com.infodeli.admin.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.Query;

import com.infodeli.admin.dao.InfoImagesDao;
import com.infodeli.admin.entity.InfoImagesEntity;
import com.infodeli.admin.service.InfoImagesService;


@Service("infoImagesService")
public class InfoImagesServiceImpl extends ServiceImpl<InfoImagesDao, InfoImagesEntity> implements InfoImagesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InfoImagesEntity> page = this.page(
                new Query<InfoImagesEntity>().getPage(params),
                new QueryWrapper<InfoImagesEntity>()
        );

        return new PageUtils(page);
    }

}