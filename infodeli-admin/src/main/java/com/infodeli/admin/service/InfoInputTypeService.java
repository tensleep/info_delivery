package com.infodeli.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.admin.entity.InfoInputTypeEntity;

import java.util.List;
import java.util.Map;

/**
 * 输入种类表
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-28 18:39:24
 */
public interface InfoInputTypeService extends IService<InfoInputTypeEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageShow(Map<String, Object> params);

    void updateShowStatus(Long typeId, Integer showStatus);

    List<InfoInputTypeEntity> listAllShow();

}

