package com.infodeli.admin.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.Query;

import com.infodeli.admin.dao.InfoAttrSelectDao;
import com.infodeli.admin.entity.InfoAttrSelectEntity;
import com.infodeli.admin.service.InfoAttrSelectService;


@Service("infoAttrSelectService")
public class InfoAttrSelectServiceImpl extends ServiceImpl<InfoAttrSelectDao, InfoAttrSelectEntity> implements InfoAttrSelectService {

    @Override // 分页 查询
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InfoAttrSelectEntity> page = this.page(
                new Query<InfoAttrSelectEntity>().getPage(params),
                new QueryWrapper<InfoAttrSelectEntity>()
        );

        return new PageUtils(page);
    }

    @Override // 分页、关键词 查询
    public PageUtils queryPageByAttrId(Map<String, Object> params, Long attrId) {
        QueryWrapper<InfoAttrSelectEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("attr_id", attrId);
        String selectValue = (String) params.get("key");
        if(selectValue != null && !"".equals(selectValue)){
            wrapper.like("select_value", selectValue);
        }
        IPage<InfoAttrSelectEntity> page = this.page(
                new Query<InfoAttrSelectEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override // 更新 showStatus
    public void updateShowStatus(Long selectId, Integer showStatus) {
        InfoAttrSelectEntity entity = new InfoAttrSelectEntity();
        entity.setSelectId(selectId);
        entity.setShowStatus(showStatus);
        this.updateById(entity);
    }

    @Override
    public List<InfoAttrSelectEntity> listByAttrId(List<Long> attrIds) {
        QueryWrapper<InfoAttrSelectEntity> wrapper = new QueryWrapper<>();
        wrapper.in("attr_id", attrIds);
        return this.list(wrapper);
    }



}