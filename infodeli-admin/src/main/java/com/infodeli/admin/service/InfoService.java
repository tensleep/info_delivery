package com.infodeli.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.infodeli.admin.vo.InfoVo;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.admin.entity.InfoEntity;

import java.util.List;
import java.util.Map;

/**
 * 信息
 *
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
public interface InfoService extends IService<InfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage1(Map<String, Object> params);

    PageUtils queryPageVo(Map<String, Object> params); // 分页获取 InfoVo

    InfoVo getVoById(Long infoId); // 获取一个 InfoVo

    void userSave(InfoVo vo, Long uid);

    void updateInfoStatus(List<InfoVo> infoVos);

    void addCommentNum(Long infoId, Integer num);


    void updateVo(InfoVo info); // 更新 InfoVo
}

