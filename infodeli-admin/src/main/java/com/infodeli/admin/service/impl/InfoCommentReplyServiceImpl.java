package com.infodeli.admin.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.infodeli.admin.entity.InfoEntity;
import com.infodeli.admin.service.InfoCommentService;
import com.infodeli.admin.service.InfoService;
import com.infodeli.admin.vo.InfoCommentReplyVo;
import com.infodeli.common.utils.ComTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.infodeli.common.utils.PageUtils;
import com.infodeli.common.utils.Query;

import com.infodeli.admin.dao.InfoCommentReplyDao;
import com.infodeli.admin.entity.InfoCommentReplyEntity;
import com.infodeli.admin.service.InfoCommentReplyService;


@Service("infoCommentReplyService")
public class InfoCommentReplyServiceImpl extends ServiceImpl<InfoCommentReplyDao, InfoCommentReplyEntity> implements InfoCommentReplyService {

    @Autowired
    InfoCommentService infoCommentService;

    @Autowired
    InfoService infoService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InfoCommentReplyEntity> page = this.page(
                new Query<InfoCommentReplyEntity>().getPage(params),
                new QueryWrapper<InfoCommentReplyEntity>()
        );

        return new PageUtils(page);
    }

    @Override // 分页查询 回复
    public PageUtils queryPageByCommentId(Map<String, Object> params, Long commentId) {
        QueryWrapper<InfoCommentReplyVo> wrapper = new QueryWrapper<>();
        wrapper.eq("comment_id", commentId).eq("show_status", 1);
        IPage<InfoCommentReplyVo> infoCommentReplyVos = baseMapper.queryPageVo(
                new Query<InfoCommentReplyVo>().getPage(params), wrapper
        );
        return new PageUtils(infoCommentReplyVos);
    }

    /*
{
    fromId: ,
    toId: ,
    commentId: ,
    infoId: ,
    replyType: ,
    replyContent: ,
}

    * */
    @Override // 对 评论 进行 回复
    public void reply(InfoCommentReplyEntity entity) {
        entity.setLikeNum(0);
        entity.setHateNum(0);
        entity.setCreateTime(ComTool.getDateStr());
        entity.setShowStatus(1);
        if(entity.getFromId() == entity.getToId()){
            entity.setReadStatus(1);
        }else{
            entity.setReadStatus(0);
        }
        this.save(entity);
        // 增加 info的comment_num 以及 comment的reply_num
        infoCommentService.addReplyNum(entity.getInfoId(), entity.getCommentId(), 1);
    }

    /**
     *
     * @param replyId 删除某个回复
     * @param uid 删除的发起者 【只允许这两种人删回复：1、info的发布者，2、reply的from】
     * @param infoId 删除的回复 对应的 消息
     * @param commentId 删除的回复 对应的 评论
     */
    @Override
    public void deleteLogic(Long replyId, Long uid, Long infoId, Long commentId){
        InfoEntity byId = infoService.getById(infoId);
        QueryWrapper<InfoCommentReplyEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("reply_id", replyId).eq("info_id", infoId).eq("comment_id", commentId);
        if(byId == null || byId.getUserId() != uid){
            wrapper.eq("from_id", uid);
        }
        Integer num = baseMapper.deleteLogic(wrapper);
        // 更新 info 的 commentNum 以及 comment 的 replyNum
        if(num == null || num == 0){
            System.out.println("没有reply需要删除"+replyId);
            return ;
        }
        infoCommentService.addReplyNum(infoId, commentId, -num);
    }

}