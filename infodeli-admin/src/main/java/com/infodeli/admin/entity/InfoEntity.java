package com.infodeli.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 信息
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@Data
@TableName("info")
public class InfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 信息Id
	 */
	@TableId
	private Long infoId;
	/**
	 * 所属分类
	 */
	private Long catId;
	/**
	 * 标题
	 */
	private String infoTitle;
	/**
	 * 默认图片
	 */
	private String infoDefaultImg;
	/**
	 * 信息状态[0-待,1-审核,2-成功,-1-删,-2-失败]
	 */
	private Integer infoStatus;
	/**
	 * 匿名发布[0-否,1-是]
	 */
	private Integer anon;
	/**
	 * 上传者id
	 */
	private Long userId;
	/**
	 * 创建时间
	 */
	private String createTime;

	/**
	 * 评论数
	 */
	private Integer commentNum;

}
