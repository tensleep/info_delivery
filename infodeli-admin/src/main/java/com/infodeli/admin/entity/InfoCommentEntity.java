package com.infodeli.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 信息评论表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-10 18:06:20
 */
@Data
@TableName("info_comment")
public class InfoCommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 评论id
	 */
	@TableId
	private Long commentId;
	/**
	 * 消息id
	 */
	private Long infoId;
	/**
	 * 用户id
	 */
	private Long uid;
	/**
	 * 被评论用户id
	 */
	private Long toId;
	/**
	 * 评论内容
	 */
	private String commentContent;
	/**
	 * 回复数
	 */
	private Integer replyNum;
	/**
	 * 喜欢
	 */
	private Integer likeNum;
	/**
	 * 讨厌
	 */
	private Integer hateNum;
	/**
	 * 创建时间
	 */
	private String createTime;
	/**
	 * 是否显示
	 */
	private Integer showStatus;
	/**
	 * 是否已读
	 */
	private Integer readStatus;

}
