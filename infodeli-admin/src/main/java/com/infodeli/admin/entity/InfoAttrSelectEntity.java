package com.infodeli.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 属性可选值表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-28 18:39:24
 */
@Data
@TableName("info_attr_select")
public class InfoAttrSelectEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 可选值id
	 */
	@TableId
	private Long selectId;
	/**
	 * 属性id
	 */
	private Long attrId;
	/**
	 * 可选值名称
	 */
	private String selectValue;
	/**
	 * 是否显示[0-否,1是]
	 */
	private Integer showStatus;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 图标地址
	 */
	private String icon;

}
