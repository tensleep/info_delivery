package com.infodeli.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 信息评论回复表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-05-10 18:06:20
 */
@Data
@TableName("info_comment_reply")
public class InfoCommentReplyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 回复id
	 */
	@TableId
	private Long replyId;
	/**
	 * 回复用户id
	 */
	private Long fromId;
	/**
	 * 被回复用户id
	 */
	private Long toId;
	/**
	 * 评论id
	 */
	private Long commentId;
	/**
	 * 是否回复评论
	 */
	private Integer replyType;
	/**
	 * 回复内容
	 */
	private String replyContent;
	/**
	 * 喜欢
	 */
	private Integer likeNum;
	/**
	 * 讨厌
	 */
	private Integer hateNum;
	/**
	 * 创建时间
	 */
	private String createTime;
	/**
	 * 是否显示
	 */
	private Integer showStatus;

	/**
	 * 信息id
	 */
	private Long infoId;
	/**
	 * 是否已读
	 */
	private Integer readStatus;

}
