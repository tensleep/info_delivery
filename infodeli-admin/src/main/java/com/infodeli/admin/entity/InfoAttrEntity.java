package com.infodeli.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 信息属性
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-28 18:39:24
 */
@Data
@TableName("info_attr")
public class InfoAttrEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 属性id
	 */
	@TableId
	private Long attrId;
	/**
	 * 属性名
	 */
	private String attrName;
	/**
	 * 分类id
	 */
	private Long catId;
	/**
	 * 是否需要检索[0-否，1-是]
	 */
	private Integer searchType;
	/**
	 * 输入种类id
	 */
	private Long typeId;
	/**
	 * 是否显示
	 */
	private Integer showStatus;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 属性图标
	 */
	private String icon;

	@TableField(exist = false)
	private List<Long> catIdPath;

}
