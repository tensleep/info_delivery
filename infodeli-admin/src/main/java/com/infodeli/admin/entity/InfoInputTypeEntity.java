package com.infodeli.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 输入种类表
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-28 18:39:24
 */
@Data
@TableName("info_input_type")
public class InfoInputTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 输入种类id
	 */
	@TableId
	private Long typeId;
	/**
	 * 输入种类名称
	 */
	private String typeName;
	/**
	 * 是否显示[0-否,1是]
	 */
	private Integer showStatus;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 图标地址
	 */
	private String icon;

}
