package com.infodeli.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 信息属性值
 * 
 * @author zzs
 * @email zzs@infodeli.com
 * @date 2021-04-04 22:41:53
 */
@Data
@TableName("info_attr_value")
public class InfoAttrValueEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long valId;
	/**
	 * 信息id
	 */
	private Long infoId;
	/**
	 * 属性id
	 */
	private Long attrId;
	/**
	 * 属性名
	 */
	private String attrName;
	/**
	 * 属性值
	 */
	private String attrValue;

	/**
	 * 输入类型
	 */
	private String type;

}
