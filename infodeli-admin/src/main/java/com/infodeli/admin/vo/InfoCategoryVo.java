package com.infodeli.admin.vo;

import com.infodeli.admin.entity.InfoCategoryEntity;
import lombok.Data;

import java.util.List;

@Data
public class InfoCategoryVo {
    /**
     * 分类id
     */
    private Long catId;
    /**
     * 分类名称
     */
    private String catName;
    /**
     * 父分类id
     */
    private Long parentCid;
    /**
     * 层级
     */
    private Integer catLevel;
    /**
     * 是否显示[0-否,1是]
     */
    private Integer showStatus;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 图标地址
     */
    private String icon;


}
