package com.infodeli.admin.vo;

import lombok.Data;


import java.util.List;

@Data
public class InfoAttrVo {

    private Long attrId;
    /**
     * 属性名
     */
    private String attrName;
    /**
     * 分类id
     */
    private Long catId;
    /**
     * 是否需要检索[0-否，1-是]
     */
    private Integer searchType;
    /**
     * 输入种类id
     */
    private Long typeId;
    /**
     * 是否显示
     */
    private Integer showStatus;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 属性图标
     */
    private String icon;

    private List<Long> catIdPath;
    private String type;
    private List<String> selectValue;

    private String attrValue = ""; // 前端提交新信息时使用
}
