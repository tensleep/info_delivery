package com.infodeli.admin.vo;

import com.infodeli.admin.entity.InfoAttrValueEntity;
import lombok.Data;

import java.util.List;

@Data
public class InfoVo {
    private Long infoId;
    /**
     * 所属分类
     */
    private Long catId;
    /**
     * 标题
     */
    private String infoTitle;
    /**
     * 默认图片
     */
    private String infoDefaultImg;
    /**
     * 信息状态[0-待,1-审核,2-成功,-1-删,-2-失败]
     */
    private Integer infoStatus;
    /**
     * 匿名发布[0-否,1-是]
     */
    private Integer anon;
    /**
     * 上传者id
     */
    private Long userId;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 评论数
     */
    private Integer commentNum;

    private List<InfoAttrVo> infoAttrVos; // 用户上传时使用

    private List<InfoAttrValueVo> infoAttrValueVos; // 获取信息时使用

    private List<String> img;

    private String nickName;

    private String portrait;

}
