package com.infodeli.admin.vo;

import lombok.Data;

import java.util.List;

@Data
public class InfoAttrValueVo {

    /**
     * id
     */
    private Long valId;
    /**
     * 信息id
     */
    private Long infoId;
    /**
     * 属性id
     */
    private Long attrId;
    /**
     * 属性名
     */
    private String attrName;
    /**
     * 属性值
     */
    private String attrValue;

    /**
     * 输入类型
     */
    private String type;

    /**
     * 可选值
     */
    private List<String> selectValue;

    /**
     * 排序
     */
    private Integer sort;
}
