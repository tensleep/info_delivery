package com.infodeli.admin.vo;

import lombok.Data;

@Data
public class InfoCommentReplyVo {
    /**
     * 回复id
     */
    private Long replyId;
    /**
     * 回复用户id
     */
    private Long fromId;
    /**
     * 被回复用户id
     */
    private Long toId;
    /**
     * 评论id
     */
    private Long commentId;
    /**
     * 是否回复评论
     */
    private Integer replyType;
    /**
     * 回复内容
     */
    private String replyContent;
    /**
     * 喜欢
     */
    private Integer likeNum;
    /**
     * 讨厌
     */
    private Integer hateNum;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 是否显示
     */
    private Integer showStatus;
    /**
     * 信息id
     */
    private Long infoId;
    /**
     * 是否已读
     */
    private Integer readStatus;

    // 用户相关
    private String fromNickName;
    private String fromPortrait;
    private String toNickName;
}
