package com.infodeli.admin.vo;

import lombok.Data;

@Data
public class InfoCommentVo {
    /**
     * 评论id
     */
    private Long commentId;
    /**
     * 消息id
     */
    private Long infoId;
    /**
     * 用户id
     */
    private Long uid;
    /**
     * 被评论用户id
     */
    private Long toId;
    /**
     * 评论内容
     */
    private String commentContent;
    /**
     * 回复数
     */
    private Integer replyNum;
    /**
     * 喜欢
     */
    private Integer likeNum;
    /**
     * 讨厌
     */
    private Integer hateNum;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 是否显示
     */
    private Integer showStatus;
    /**
     * 是否已读
     */
    private Integer readStatus;

    // 与用户相关的信息
    private String nickName;
    private String portrait;
}
