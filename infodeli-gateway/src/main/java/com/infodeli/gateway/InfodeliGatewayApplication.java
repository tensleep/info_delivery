package com.infodeli.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfodeliGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfodeliGatewayApplication.class, args);
	}

}
